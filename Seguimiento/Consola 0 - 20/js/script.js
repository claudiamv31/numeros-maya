const nivel1 = 1;
const nivel2 = 20;

function divisionVeinte(dividendo){
    let resultado = dividendo/nivel2;
    return Math.trunc(resultado);
}

function repartirNumeroVeinte(){
    let resultado = 0;
    let numero = document.getElementById('numero').value;
    let cociente = divisionVeinte(numero);
        if(cociente > 0){
            let multiplicacion = nivel2 * cociente;
            resultado = numero - multiplicacion;
        }
        else{
            resultado = numero;
        }
    return resultado;
}

function cambiarValor(nivel, valorNivel){
    console.log(nivel + ' Valor: ' + valorNivel);
}

function mostrarResultadoNivel2(){
    let numero = document.getElementById('numero').value;
    let valorNivel = divisionVeinte(numero);
    if(numero <= 20){
        if(valorNivel > 0){
        cambiarValor('nivel2', valorNivel);
        }
        else{
        console.log('');
        }
        mostrarResultadoNivel1();
    }
    else{
    alert('El valor es mayor a 20');
    }
}

function mostrarResultadoNivel1(){
    let valorNivel = repartirNumeroVeinte();
    cambiarValor('nivel1', valorNivel);
}

function limpiar(){
    for(x = 1; x < 4; x++){
        console.log('');
    }
}



