const nivel1 = 1;
const nivel2 = 20;
const nivel3 = 400;

function divisionCuatrocientos(dividendo){
    let resultado = dividendo/nivel3;
    return Math.trunc(resultado);
}

function divisionVeinte(dividendo){
    let resultado = dividendo/nivel2;
    return Math.trunc(resultado);
}

function repartirNumeroCuatrocientos(){
    let numero = document.getElementById('numero').value;
    let resultado = 0;
    let cociente = divisionCuatrocientos(numero);
    if(cociente > 0){
        var multiplicacion = nivel3 * cociente;
        resultado = numero - multiplicacion;
    }
    else{
        resultado = numero;
    }
    return resultado;
}

function repartirNumeroVeinte(){
    let resultado = 0;
    let numeroRestante = repartirNumeroCuatrocientos();
    let cociente = divisionVeinte(numeroRestante);
    if(cociente > 0){
        let multiplicacion = nivel2 * cociente;
        resultado = numeroRestante - multiplicacion;
    }
    else{
        resultado = numeroRestante;
    }
    return resultado;
}

function cambiarImagenes(nivel, valorNivel){
    console.log(nivel + ' Valor: ' + valorNivel);
}

function mostrarResultadoNivel3(){
    let numero = document.getElementById('numero').value;
    let valorNivel = divisionCuatrocientos(numero);
    if(numero % 1 == 0){
        if(numero <= nivel3){
            if(valorNivel > 0){
                cambiarImagenes('nivel3', valorNivel);
            }
            else{
                console.log('');
            }
            mostrarResultadoNivel2();
            mostrarResultadoNivel1();
        }
        else{
            alert('El numero es mayor a 400');
        }
    }
    else{
        alert('El numero no es entero');
    }
}

function mostrarResultadoNivel2(){
    let numero = document.getElementById('numero').value;
    let valorNivel = divisionVeinte(repartirNumeroCuatrocientos());
    if(divisionCuatrocientos(numero) > 0){
        cambiarImagenes('nivel2', valorNivel);
    }
    else if(valorNivel > 0){
        cambiarImagenes('nivel2', valorNivel);
    }
    else{
        console.log('');
    }
}

function mostrarResultadoNivel1(){
    let valorNivel = repartirNumeroVeinte();
    cambiarImagenes('nivel1', valorNivel);
}


